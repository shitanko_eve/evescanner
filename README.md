# README #

This is a fork of https://bitbucket.org/viktorielucilla/evescanner-net4. Unfortunatly Viktorie\_Lucilla stopped playing EVE and working on this tool. When evepraisal changed their code this did not work anymore. Thankfully Viktorie\_Lucilla put this under LGPL 3.0, so I can try to fix this.

### What is this repository for? ###

* Scan potential targets, post their information for everyone to see.

### How do I get set up? ###

* Requires Visual Studio 2013, .net 4.0 Client Profile

### Who do I talk to? ###

* GSF forum or ingame: Shitanko
* Or even better, just create an issue or a pull request here

### Thanks ###
Thanks go to Viktorie\_Lucilla for the creation of this tool

### License ###
This project is licensed by default under the LGPL 3.0. If you desire an alternate license, please contact me at my e-mail address above. I've been out of EVE most of the time lately, so, I may not get back to you immediately.