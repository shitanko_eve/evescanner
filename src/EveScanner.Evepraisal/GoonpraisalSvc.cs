﻿namespace EveScanner.Evepraisal
{
    /// <summary>
    /// Stub class to turn EvepraisalSvc into GoonpraisalSvc
    /// </summary>
    public class GoonpraisalSvc : EvepraisalSvc
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GoonpraisalSvc"/> class.
        /// </summary>
        public GoonpraisalSvc() : base("www.goonpraisal.com", true)
        {
        }
    }
}
