﻿//-----------------------------------------------------------------------
// <copyright company="Viktorie Lucilla" file="EvepraisalTotals.cs">
// Copyright © Viktorie Lucilla 2015. All Rights Reserved
// </copyright>
//-----------------------------------------------------------------------
namespace EveScanner.Evepraisal
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Holds meta data
    /// </summary>
    [DataContract]
    public class EvepraisalMeta
    {
        /// <summary>
        /// Is this a BPC?
        /// </summary>
        [DataMember(Name = "bpc")]
        public bool BPC { get; set; }

        /// <summary>
        /// Gets or sets the overall Sell price.
        /// </summary>
        [DataMember(Name = "bpcRuns")]
        public int BPCRuns { get; set; }

    }
}
