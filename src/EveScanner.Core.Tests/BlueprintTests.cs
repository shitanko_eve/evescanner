﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace EveScanner.Core.Tests
{
    [TestFixture]
    public class BlueprintTests
    {
        private ScanRuleEvaluator eve;

        [SetUp]
        public void SetUp()
        {
            eve = GetRulesFile();
            Assert.IsNotNull(eve);
        }

        [Test]
        public void TestBlueprintRule1()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Hulk Blueprint", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Hulk Blueprint", false){IsBlueprint = true, IsBlueprintCopy = false} }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNotNull(result);
            Assert.AreEqual("tag", result.ResultType);
            Assert.AreEqual("Hulk BPO", result.ResultValue);           
        }
        
        [Test]
        public void TestBlueprintRule2()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Hulk Blueprint", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Hulk Blueprint", false){IsBlueprint = true, IsBlueprintCopy = true} }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNull(result);
        }
                
        [Test]
        public void TestBlueprintRule3()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Arc Blueprint", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Ark Blueprint", false){IsBlueprint = true, IsBlueprintCopy = true} }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNotNull(result);
            Assert.AreEqual("tag", result.ResultType);
            Assert.AreEqual("Arc BPC", result.ResultValue);           
        }
        
        [Test]
        public void TestBlueprintRule4()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Arc Blueprint", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Ard Blueprint", false){IsBlueprint = true, IsBlueprintCopy = false} }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNull(result);
        }
        
        private static ScanRuleEvaluator GetRulesFile()
        {
            ScanRuleEvaluator eve = new ScanRuleEvaluator(TestContext.CurrentContext.TestDirectory + "\\rules2.xml");
            Assert.IsNotNull(eve);
            return eve;
        }

    }
}