﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace EveScanner.Core.Tests
{
    [TestFixture]
    public class RuleBasedRules
    {
        private ScanRuleEvaluator eve;

        [SetUp]
        public void SetUp()
        {
            eve = GetRulesFile();
            Assert.IsNotNull(eve);
        }

        [Test]
        public void TestAnyRule1()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Dummy Item\r\n1 Dummy Item 2", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Dummy Item", false), new ScanLine(1, "Dummy Item 2", false) }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNotNull(result);
            Assert.AreEqual("tag", result.ResultType);
            Assert.AreEqual("Rule Based Sample Rule Any", result.ResultValue);           
        }
        
        [Test]
        public void TestAnyRule2()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Dummy Item", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Dummy Item", false) }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNotNull(result);
            Assert.AreEqual("tag", result.ResultType);
            Assert.AreEqual("Rule Based Sample Rule Any", result.ResultValue);           
        }
        
        [Test]
        public void TestAnyRule3()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Dummy Item 2", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Dummy Item 2", false) }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNotNull(result);
            Assert.AreEqual("tag", result.ResultType);
            Assert.AreEqual("Rule Based Sample Rule Any", result.ResultValue);           
        }
        
        [Test]
        public void TestAllRule1()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Dummy Item All\r\n1 Dummy Item All 2", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Dummy Item All", false), new ScanLine(1, "Dummy Item All 2", false) }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNotNull(result);
            Assert.AreEqual("tag", result.ResultType);
            Assert.AreEqual("Rule Based Sample Rule All", result.ResultValue);           
        }
        
        [Test]
        public void TestAllRule2()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Dummy Item All", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Dummy Item All", false) }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNull(result);
        }
        
        [Test]
        public void TestAllRule3()
        {
            ScanResult r = new ScanResult(Guid.Empty, DateTime.Now, "1 Dummy Item All 2", 3000000000000, 4123456789012, 1, 1, "http://goonfleet.com/?1", new[] { new ScanLine(1, "Dummy Item All 2", false) }) { CharacterName = "Viktorie Lucilla", ShipType = "Providence - Freighter - Amarr", Notes = "Triggers T2 BPO Image", Location = "Perimeter -> Urlen" };
            IEnumerable<EvaluationResult> output = eve.Evaluate(r);
            EvaluationResult result = output.ElementAtOrDefault(0);
            Assert.IsNull(result);
        }
        
        private static ScanRuleEvaluator GetRulesFile()
        {
            ScanRuleEvaluator eve = new ScanRuleEvaluator(TestContext.CurrentContext.TestDirectory + "\\rules2.xml");
            Assert.IsNotNull(eve);
            return eve;
        }

    }
}